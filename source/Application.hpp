#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include "Context.hpp"
#include "Settings.hpp"

#include "states/StateBase.hpp"
#include "renderer/RenderMaster.hpp"

class Application {

public:
	Application(Settings& settings);

	void runGameLoop();

	void popState();

	template<typename S, typename... Args>
	void pushState(Args&&... args) {
		m_states.push_back(std::make_unique<S>(std::forward<Args&>(args)...));
	};

	const sf::RenderWindow& getWindow() const;

private:
	void handleEvents();

	Context m_context;
	RenderMaster m_renderMaster;

	StateBase& getCurrentState();
	std::vector<std::unique_ptr<StateBase>> m_states;

	bool m_shouldPop = false;
};


#endif //APPLICATION_HPP

#include "Context.hpp"

#include <GL/glew.h>

#include "Config.hpp"

sf::RenderWindow* g_window;

Context::Context(const Settings& settings) {
	// Создаем переменную с параметрами контеrста OpenGL(depthBits, stenciBits, antialiasingLevel, majorVersion. minorVersion)
	sf::ContextSettings contextSettings(24, 8, settings.antialiasing, 3, 3);

	// Создаем окно
	if (settings.isFullscreen) {
		window.create(sf::VideoMode::getDesktopMode(), PROJECT_NAME " - " VERSION, sf::Style::Fullscreen, contextSettings);
	} else {
		window.create(sf::VideoMode(settings.windowX, settings.windowY), PROJECT_NAME " - " VERSION, sf::Style::Close, contextSettings);
	}

	// Вкл/Выкл вертикальную синхронизацию
	if (settings.verticalSync) {
		window.setVerticalSyncEnabled(true);
	} else {
		window.setVerticalSyncEnabled(false);
	}

	// Указываем ограничение по фпс
	window.setFramerateLimit(120);

	g_window = &window;

	/*// Иницилизируем GLEW
	glewExperimental = GL_TRUE;
	GLenum glew = glewInit();
	if (glew != GLEW_OK) {
		throw std::runtime_error("GLEW init failed.");
	}*/

	// Создаем и отрисовываем содержимое окна viewport(x, y, width, height)
	glViewport(0, 0, window.getSize().x, window.getSize().y);

	// Отключаем отрисовку задней части приметивов
	glCullFace(GL_BACK);

	// Включаем смешивание цветов (Альфа-канал/Прозрачность)
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

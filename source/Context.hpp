#ifndef CONTEXT_HPP
#define CONTEXT_HPP

#include <SFML/Graphics.hpp>

#include "Settings.hpp"

struct Context {

	Context(const Settings& settings);

	sf::RenderWindow window;

};

extern sf::RenderWindow* g_window;

#endif //CONTEXT_HPP

#include <fstream>
#include <iostream>

#include "Settings.hpp"
#include "Application.hpp"

extern "C" {
	__declspec(dllexport) bool NvOptimusEnablement = true;
	__declspec(dllexport) bool AmdPowerXpressRequestHighPerformance = true;
}

void loadSettings(Settings& settings) {
	std::string key;
	std::ifstream configFile("settings.txt");

	if (configFile.is_open()) {
		while (configFile >> key) {
			if (key == "fullscreen") {
				configFile >> settings.isFullscreen;
				std::cout << "Config: Full screen mode - " << std::boolalpha << settings.isFullscreen << std::endl;
			} else if (key == "verticalsync") {
				configFile >> settings.verticalSync;
				std::cout << "Config: Vertical Syns mode - " << std::boolalpha << settings.verticalSync << std::endl;
			} else if (key == "antiliasing") {
				configFile >> settings.antialiasing;
				std::cout << "Config: Antiliasing level - "  << settings.antialiasing << std::endl;
			} else if (key == "windowsize") {
				configFile >> settings.windowX >> settings.windowY;
				std::cout << "Config: Windows size - "  << settings.windowX << "x" << settings.windowY << std::endl;
			}
		}
	} else {
		std::cout << "Config: Could not find config.txt file! Using defaults." << std::endl;
	}
}


int main(int argc, char* argv[]) {
	try {
		Settings settings;
		loadSettings(settings);

		Application application(settings);
		application.runGameLoop();

		return EXIT_SUCCESS;
	} catch (std::exception& error) {
		std::cout << "Error: " << error.what() << std::endl;
		return EXIT_FAILURE;
	}
}
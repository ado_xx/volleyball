#include "Background.hpp"

#include "../Context.hpp"
#include "../renderer/RenderMaster.hpp"

namespace Interface {

Background::Background(const sf::Texture &texture) {
	m_quad.setSize({static_cast<float>((*g_window).getSize().x), static_cast<float>((*g_window).getSize().y)});
	m_quad.setTexture(&texture);
}

void Background::render(RenderMaster &renderer) noexcept {
	renderer.drawSFML(m_quad);
}

}

#ifndef BACKGROUND_HPP
#define BACKGROUND_HPP

#include <SFML/Graphics.hpp>

class RenderMaster;

namespace Interface {

class Background {

public:
	Background(const sf::Texture& texture);

	void render(RenderMaster& renderer) noexcept;

private:
	sf::RectangleShape m_quad;

};

}


#endif //BACKGROUND_HPP

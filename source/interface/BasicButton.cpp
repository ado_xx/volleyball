#include "BasicButton.hpp"

#include "../renderer/RenderMaster.hpp"

namespace Interface {

BasicButton::BasicButton(const std::wstring& text, std::function<void(void)> function)
:	Button(m_quad)
,	m_function(function) {
	m_quad.setSize({250, 100});
	m_quad.setFillColor(sf::Color(255,255,255,3));

	initText(m_text, 24, text);
}

void BasicButton::onClick() {
	m_function();
}

void BasicButton::onNoInteract() {
	m_quad.setFillColor(sf::Color(255,255,255,3));
}

void BasicButton::onMouseTouch() {
	m_quad.setFillColor(sf::Color(0, 204, 209, 127));
}

void BasicButton::render(RenderMaster &renderer) noexcept {
	renderer.drawSFML(m_quad);
	renderer.drawSFML(m_text);
}

void BasicButton::setPosition(const sf::Vector2f &position) {
	m_quad.setPosition(position);
	m_text.setPosition(position);
	m_text.move(
			m_quad.getSize().x / 2 - m_text.getLocalBounds().width / 2,
			m_quad.getSize().y / 2 - m_text.getLocalBounds().height / 2
	);
}

const sf::Vector2f BasicButton::getSize() const {
	return m_quad.getSize();
}

}

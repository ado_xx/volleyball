#include "Button.hpp"

namespace Interface {

Button::Button(const sf::RectangleShape& rectangle) : m_rectangle(rectangle) {}

void Button::update(float deltaTime) {}

void Button::handleEvent(const sf::Event& event) {
	if (touchingMouse(m_rectangle)) {
		onMouseTouch();
	} else {
		onNoInteract();
	}

	if (clicked(m_rectangle, event)) {
		onClick();
	}
}

}


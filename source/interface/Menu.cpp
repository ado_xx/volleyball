#include "Menu.hpp"

#include "../Context.hpp"
#include "../utils/ResourceHolder.hpp"

namespace Interface {

Menu::Menu() : m_background(ResourceHolder::getTexture("background")) {

}

void Menu::addComponent(std::unique_ptr<Component> component) {
	(*component).setPosition({(*g_window).getSize().x / 2 - (*component).getSize().x / 2, padding});
	padding += (*component).getSize().y + 20;
	m_components.push_back(std::move(component));
}

void Menu::update(float deltaTime) {
	for (auto& component : m_components) {
		(*component).update(deltaTime);
	}
}

void Menu::handleEvent(const sf::Event& event) {
	for (auto& component : m_components) {
		(*component).handleEvent(event);
	}
}

void Menu::render(RenderMaster& renderer) {
	m_background.render(renderer);
	for (auto& component : m_components) {
		(*component).render(renderer);
	}
}

void Menu::clear() {
	m_components.clear();
	m_components.shrink_to_fit();
}

}

#ifndef MENU_HPP
#define MENU_HPP

#include "Component.hpp"
#include "Background.hpp"

namespace Interface {

class Menu {

public:
	Menu();

	void addComponent(std::unique_ptr<Component> component);

	void update(float deltaTime);
	void handleEvent(const sf::Event& event);
	void render(RenderMaster& renderer);

	void clear();

private:
	Background m_background;

	float padding = 100;
	std::vector<std::unique_ptr<Component>> m_components;

};

}

#endif //MENU_HPP

#include "RenderMaster.hpp"

#include <GL/glew.h>

RenderMaster::RenderMaster() {}

void RenderMaster::render(sf::RenderWindow& window) {
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	m_sfmlRenderer.render(window);

	window.display();
}

void RenderMaster::drawSFML(const sf::Drawable& drawable) {
	m_sfmlRenderer.add(drawable);
}

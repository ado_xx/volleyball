#ifndef RENDERMASTER_HPP
#define RENDERMASTER_HPP

#include <SFML/Graphics.hpp>

#include "SFMLRenderer.hpp"

class RenderMaster {

public:
	RenderMaster();

	void render(sf::RenderWindow& window);

	void drawSFML(const sf::Drawable& drawable);

private:
	SFMLRenderer m_sfmlRenderer;

};


#endif //RENDERMASTER_HPP

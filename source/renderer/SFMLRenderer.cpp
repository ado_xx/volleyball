#include "SFMLRenderer.hpp"

#include <GL/glew.h>

SFMLRenderer::SFMLRenderer() {}

void SFMLRenderer::add(const sf::Drawable& drawable) {
	m_drawables.push_back(&drawable);
}

void SFMLRenderer::render(sf::RenderWindow& window) {
	if (m_drawables.empty()) {
		return;
	}

	// Выключаем тест глубины
	glDisable(GL_DEPTH_TEST);

	window.pushGLStates();
	window.resetGLStates();

	for (const auto& draw : m_drawables) {
		window.draw(*draw);
	}

	window.popGLStates();

	m_drawables.clear();
}

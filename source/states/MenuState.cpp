#include "MenuState.hpp"

#include "../Context.hpp"
#include "../Application.hpp"

#include "../interface/Image.hpp"
#include "../interface/BasicButton.hpp"
#include "../utils/ResourceHolder.hpp"

#include "PlayingState.hpp"

MenuState::MenuState(Application& application, const Settings& settings) : StateBase(application) {

	m_menuMain.addComponent(std::make_unique<Interface::Image>("logotipe"));

	m_menuMain.addComponent(std::make_unique<Interface::BasicButton>(L"НАЧАТЬ ИГРУ", [&] () {
		application.pushState<PlayingState>(application, settings);
	}));

	m_menuMain.addComponent(std::make_unique<Interface::BasicButton>(L"ИНФОРМАЦИЯ ОБ ИГРЕ", [&]() {
		m_activeMenu = &m_menuSetting;
	}));

	m_menuMain.addComponent(std::make_unique<Interface::BasicButton>(L"ВЫХОД", [&]() {
		(*g_window).close();
	}));

	m_menuSetting.addComponent(std::make_unique<Interface::BasicButton>(L"НАЗАД", [&]() {
		m_activeMenu = &m_menuMain;
	}));

    Background.setSize(sf::Vector2f(500,450));
    Background.setOrigin(250,275);
    Background.setPosition(g_window->getSize().x/2 , g_window->getSize().y/2+120);
    Background.setTexture(&ResourceHolder::getTexture("background5"));
}

MenuState::~MenuState() {

}

void MenuState::update(float deltaTime) {
	(*m_activeMenu).update(deltaTime);
}

void MenuState::handleEvent(sf::Event event) {
	(*m_activeMenu).handleEvent(event);
}

void MenuState::render(RenderMaster& renderer) {
	(*m_activeMenu).render(renderer);
    if (m_activeMenu == &m_menuSetting)
     renderer.drawSFML(Background);
}
#ifndef MENUSTATE_HPP
#define MENUSTATE_HPP

#include <thread>
#include <memory>

#include "StateBase.hpp"
#include "../Settings.hpp"
#include "../renderer/RenderMaster.hpp"
#include "../interface/Menu.hpp"

class MenuState : public StateBase {

public:
	MenuState(Application& application, const Settings& settings);
	~MenuState();

	void update(float deltaTime) override;

	void handleEvent(sf::Event event) override;

	void render(RenderMaster& renderer) override;

private:
    sf::RectangleShape Background;
	Interface::Menu m_menuMain;
	Interface::Menu m_menuSetting;
	Interface::Menu* m_activeMenu = &m_menuMain;

};


#endif //MENUSTATE_HPP

#include "PlayingState.hpp"
#include "../utils/ResourceHolder.hpp"
#include "../Context.hpp"

#include "../Application.hpp"
#include "../interface/BasicButton.hpp"
#include "../utils/Random.hpp"
#include <iostream>
#include <math.h>

PlayingState::PlayingState(Application& application, const Settings& settings)
: StateBase(application)
, m_background(ResourceHolder::getTexture("background")) {

    speedPlayer = 450;
    ballAngle = 45; ballSpeed = 550;

    radiusPlayer = 50;

    mPlayer1 = {false, false, false, false, 0, 10};
    mPlayer2 = {false, false, false, false, 0, 10};

    playerone.setRadius(radiusPlayer);
    playerone.setOrigin(radiusPlayer,radiusPlayer);

    playertwo = playerone;

    playerone.setFillColor(sf::Color::Red);
    playerone.setPosition(100,625);

    playertwo.setFillColor(sf::Color::Blue);
    playertwo.setPosition(g_window->getSize().x - 100,625);

    ball.setTexture(&ResourceHolder::getTexture("ball"));
    ball.setRadius(40);
    ball.setOrigin(40,40);
    ball.setPosition(g_window->getSize().x / 2 ,310);

    setka.setSize(sf::Vector2f(10,288));
    setka.setPosition(g_window->getSize().x / 2 - 5, g_window->getSize().y - 325);
    setka.setTexture(&ResourceHolder::getTexture("setka2"));

    scorefon1.setSize(sf::Vector2f(300,60));
    scorefon1.setPosition({100,25});
    scorefon1.setFillColor(sf::Color(138,244,252,100));

    scorefon2.setSize(sf::Vector2f(300,60));
    scorefon2.setPosition({880,25});
    scorefon2.setFillColor(sf::Color(138,244,252,100));

    scoretext1.setString(L"1 Игрок  |");
    scoretext1.setOutlineThickness(2);
    scoretext1.setOutlineColor(sf::Color::Black);
    scoretext1.setPosition({120,30});
    scoretext1.setFont(ResourceHolder::getFont("OpenSans"));

    scoretext2.setString(L"2 Игрок  |");
    scoretext2.setOutlineThickness(2);
    scoretext2.setOutlineColor(sf::Color::Black);
    scoretext2.setPosition({900,30});
    scoretext2.setFont(ResourceHolder::getFont("OpenSans"));

    winertext.setOutlineThickness(2);
    winertext.setOrigin(150,275);
    winertext.setOutlineColor(sf::Color::Black);
    winertext.setPosition(g_window->getSize().x/2 , g_window->getSize().y/2+120);
    winertext.setFont(ResourceHolder::getFont("OpenSans"));

    winer.setSize(sf::Vector2f(450,400));
    winer.setOrigin(250,275);
    winer.setPosition(g_window->getSize().x/2+25 , g_window->getSize().y/2+120);
    winer.setTexture(&ResourceHolder::getTexture("winer2"));


}

PlayingState::~PlayingState() {}

void PlayingState::update(float deltaTime) {

    if (ball.getPosition().x < (*g_window).getSize().x / 2){counting2 = 0;Playerball1=1;Playerball2=0;}
    if (ball.getPosition().x > (*g_window).getSize().x / 2){counting1 = 0;Playerball1=0;Playerball2=1;}

    if (score1>=10){
        winertext.setString(L"1 Игрок Победил");}
    if (score2>=10){
        winertext.setString(L"2 Игрок Победил");}


    std::wstring sscore1 = L"1 Игрок  | " + std::to_wstring(score1);
    std::wstring sscore2 = L"2 Игрок  | " + std::to_wstring(score2);;

    scoretext1.setString(sscore1);
    scoretext2.setString(sscore2);

    sf::FloatRect ballBounds = ball.getGlobalBounds();
    sf::FloatRect setkaBounds = setka.getGlobalBounds();

    if (setkaBounds.intersects(ballBounds)){
        if (Playerball1 == 1){score2 = score2+ 1;
            ball.setPosition((*g_window).getSize().x / 2, 310);
            playerone.setPosition(100,625);
            playertwo.setPosition(g_window->getSize().x - 100,625);
            ballShot = false;
            counting1 = 0;
            counting2 = 0;
            Playerball1=0;
            Playerball1=0;
        }
        if (Playerball2 == 1) {
            score1 = score1 + 1;
            ball.setPosition((*g_window).getSize().x / 2, 310);
            playerone.setPosition(100,625);
            playertwo.setPosition(g_window->getSize().x - 100,625);
            ballShot = false;
            counting1 = 0;
            counting2 = 0;
            Playerball1 = 0;
            Playerball1 = 0;
        }
    }


    hypotinuse1 = sqrt(pow(abs(ball.getPosition().x-playerone.getPosition().x), 2) + pow(abs(ball.getPosition().y-playerone.getPosition().y), 2));
    hypotinuse2 = sqrt(pow(abs(ball.getPosition().x-playertwo.getPosition().x), 2) + pow(abs(ball.getPosition().y-playertwo.getPosition().y), 2));

	if ((ball.getPosition().x - playerone.getPosition().x) != 0) {
		Angleplayerone = atan(abs(ball.getPosition().y - playerone.getPosition().y) / (abs(ball.getPosition().x - playerone.getPosition().x))) * (180 / M_PI);
	}

	if ((ball.getPosition().x - playertwo.getPosition().x) != 0) {
		Angleplayertwo = atan(abs(ball.getPosition().y - playertwo.getPosition().y) / (abs(ball.getPosition().x - playertwo.getPosition().x))) * (180 / M_PI);
	}

/*
    std::cout << "hip1 " << hypotinuse1 << std::endl;
    std::cout << "hip2 " << hypotinuse2 << std::endl;
    std::cout << "coun1 " << counting1 << std::endl;
    std::cout << "coun2 " << counting2 << std::endl;
    std::cout << "pb1 " << Playerball1 << std::endl;
    std::cout << "pb2 " << Playerball2 << std::endl;
    std::cout << "speedPlayer " << speedPlayer << std::endl;
    std::cout << "def " << deltaTime << std::endl;
*/

    if (counting1 < 3){

            if (hypotinuse1 <= (radiusPlayer + 40)) {
                ballShot = true;
                if (ball.getPosition().x > playerone.getPosition().x) {
                    ball.setPosition(playerone.getPosition());
                    ball.move(cos(Angleplayerone * M_PI / 180) * 100, -sin(Angleplayerone * M_PI / 180) * 100);
                    ballAngle = Angleplayerone;

                    ballSpeed=600;
                    ballSpeed=450;
                } else {
                    ball.setPosition(playerone.getPosition());
                    ball.move(-cos(Angleplayerone * M_PI / 180) * 100, -sin(Angleplayerone * M_PI / 180) * 100);
                    ballAngle = 180 - Angleplayerone;

                    ballSpeed=800;
                    ballSpeed=450;
                }

                counting1 = counting1+1;
                ballStart = ball.getPosition();
                timer.restart();
            }}
    if (counting2 < 3){

            if (hypotinuse2 <= (radiusPlayer + 40)) {
                ballShot = true;
                if (ball.getPosition().x > playertwo.getPosition().x) {
                    ball.setPosition(playertwo.getPosition());
                    ball.move(cos(Angleplayertwo * M_PI / 180) * 100, -sin(Angleplayertwo * M_PI / 180) * 100);
                    ballAngle = Angleplayerone;

                    ballSpeed=800;
                    ballSpeed=450;
                } else {
                    ball.setPosition(playertwo.getPosition());
                    ball.move(-cos(Angleplayertwo * M_PI / 180) * 100, -sin(Angleplayertwo * M_PI / 180) * 100);
                    ballAngle = 180 - Angleplayertwo;

                    speedPlayer=0;
                    speedPlayer=400;
                    ballSpeed=700;
                    ballSpeed=450;
                }

                counting2 = counting2 + 1;
                ballStart = ball.getPosition();
                timer.restart();
            }}


    ////////////////////////////////////
    if (ballShot == true) {
        time = timer.getElapsedTime().asSeconds();
        ball.setPosition(
                ballStart.x + ballSpeed * cos(ballAngle * M_PI / 180) * time,
                ballStart.y - ballSpeed * sin(ballAngle * M_PI / 180) * time + 256 * pow(time, 2) / 2
        );

        if (ball.getPosition().x + ball.getOrigin().x >= (*g_window).getSize().x) {
            ball.setPosition((*g_window).getSize().x - ball.getOrigin().x - 1, ball.getPosition().y);
            ballAngle = RandomSingleton::get().intInRage(180, 235);
            ballSpeed = -ballSpeed;
            ballStart = ball.getPosition();
            timer.restart();
        }
        if (ball.getPosition().x - ball.getOrigin().x <= 0) {
            ball.setPosition(ball.getOrigin().x + 1, ball.getPosition().y);
            ballAngle = RandomSingleton::get().intInRage(315, 360);
            ballSpeed = -ballSpeed;
            ballStart = ball.getPosition();
            timer.restart();
        }

        if (ball.getPosition().y + ball.getOrigin().y >= (*g_window).getSize().y) {
            if (ball.getPosition().x > (*g_window).getSize().x / 2){score1 = score1+ 1;
                playerone.setPosition(100,625);
                playertwo.setPosition(g_window->getSize().x - 100,625);}
            if (ball.getPosition().x < (*g_window).getSize().x / 2){score2 = score2+ 1;
                playerone.setPosition(100,625);
                playertwo.setPosition(g_window->getSize().x - 100,625);}

            ball.setPosition((*g_window).getSize().x / 2, 310);
            ballShot = false;
            counting1 = 0;
            counting2 = 0;
            Playerball1=0;
            Playerball1=0;
        }



    }

    float moved = deltaTime*speedPlayer;

    /// Player One
    {
        if(playerone.getPosition().x <= g_window->getSize().x / 2 - radiusPlayer - moved)
            if (mPlayer1.right) playerone.move(moved,0);

        if(playerone.getPosition().x >= radiusPlayer + moved)
            if (mPlayer1.left)  playerone.move(-moved,0);

        if (mPlayer1.jump) {
            mPlayer1.jumptime += deltaTime;
            mPlayer1.gravy = 20 - 8.8 * mPlayer1.jumptime;

            playerone.setPosition(playerone.getPosition().x, 625 + mPlayer1.jumptime * -mPlayer1.gravy * 26);

            if (playerone.getPosition().y > 625) {
                mPlayer1.gravy = 20;
                mPlayer1.jump = false;
                mPlayer1.jumptime = 0;
                playerone.setPosition(playerone.getPosition().x, 625);
            }
        }
    }

    /// Player Two
    {
        if(playertwo.getPosition().x <= g_window->getSize().x -5- radiusPlayer - moved)
            if (mPlayer2.right) playertwo.move(moved,0);

        if(playertwo.getPosition().x >= g_window->getSize().x / 2 +5+ radiusPlayer + moved)
            if (mPlayer2.left)  playertwo.move(-moved,0);

        if (mPlayer2.jump) {
            mPlayer2.jumptime += deltaTime;
            mPlayer2.gravy = 20 - 8.8 * mPlayer2.jumptime;

            playertwo.setPosition(playertwo.getPosition().x, 625 + mPlayer2.jumptime * -mPlayer2.gravy * 26);

            if (playertwo.getPosition().y > 625) {
                mPlayer2.gravy = 20;
                mPlayer2.jump = false;
                mPlayer2.jumptime = 0;
                playertwo.setPosition(playertwo.getPosition().x, 625);
            }
        }

    }
}


void PlayingState::handleEvent(sf::Event event) {
    if (score1 <10 && score2 <10) {
        /// Player One
        {
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
                mPlayer1.right = true;
            } else {
                mPlayer1.right = false;
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
                mPlayer1.left = true;
            } else {
                mPlayer1.left = false;
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
                if (!mPlayer1.jump) mPlayer1.jump = true;
            }
        }

        /// Player Two
        {
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
                mPlayer2.right = true;
            } else {
                mPlayer2.right = false;
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
                mPlayer2.left = true;
            } else {
                mPlayer2.left = false;
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
                if (!mPlayer2.jump) mPlayer2.jump = true;
            }
        }
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
        m_application->popState();
        score1 = 0;
        score2 = 0;
        counting1 = 0;
        counting2 = 0;
        Playerball1=0;
        Playerball1=0;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::F1)) {
        ball.setPosition((*g_window).getSize().x / 2, 310);
        ballShot = false;
        score1 = 0;
        score2 = 0;
        playerone.setPosition(100,625);
        playertwo.setPosition(g_window->getSize().x - 100,625);
        counting1 = 0;
        counting2 = 0;
        Playerball1=0;
        Playerball1=0;
    }

    /*if ((hypotinuse1 <= (radiusPlayer + 40))||((hypotinuse2 <= (radiusPlayer + 40)))) {
        if (!temp1 || !temp2) {
        //if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
        ballShot = true;
        do {
            //ballAngle = RandomSingleton::get().intInRage(45, 135);
            ballAngle = Angleplayerone;
        } while (ballAngle > 80 && ballAngle < 100);
        ballStart = ball.getPosition();
        timer.restart();
        }
        temp1 = true;
        temp2 = true;
    } else {
        temp1 = false;
        temp2 = false;
    }*/
}

void PlayingState::render(RenderMaster& renderer) {
    m_background.render(renderer);
    renderer.drawSFML(playerone);
    renderer.drawSFML(playertwo);
    renderer.drawSFML(setka);
    renderer.drawSFML(ball);
    renderer.drawSFML(scorefon1);
    renderer.drawSFML(scorefon2);
    renderer.drawSFML(scoretext1);
    renderer.drawSFML(scoretext2);
    if (score1>=10 ||score2>=10){
    renderer.drawSFML(winer);
        renderer.drawSFML(winertext);
    }


}

#ifndef PLAYINGSTATE_HPP
#define PLAYINGSTATE_HPP

#include <thread>
#include <memory>

#include "../interface/BasicButton.hpp"
#include "../interface/Button.hpp"
#include "StateBase.hpp"
#include "../Settings.hpp"
#include "../renderer/RenderMaster.hpp"
#include "../interface/Background.hpp"
#include "../interface/Menu.hpp"

struct Movebled {
    bool left, right, jump, jumpdown;
    float jumptime, gravy;
};

class PlayingState : public StateBase {

public:
	PlayingState(Application& application, const Settings& settings);
	~PlayingState();

	void update(float deltaTime) override;

	void handleEvent(sf::Event event) override;

	void render(RenderMaster& renderer) override;

private:
    Interface::Background m_background;

    Movebled mPlayer1, mPlayer2;

    int speedPlayer, radiusPlayer, score1=0, score2=0,counting1=0,counting2=0, Playerball1=0,Playerball2=0;

    bool ballShot = false;
    float hypotinuse1;
    float hypotinuse2;
    float Angleplayerone,Angleplayertwo;

	float ballAngle, ballSpeed;
    float time;
    sf::Clock timer;
    sf::Vector2f ballStart;

	sf::CircleShape playerone, playertwo;

	sf::CircleShape ball;
    sf::RectangleShape setka;
    sf::RectangleShape winer;
    sf::RectangleShape scorefon1;
    sf::RectangleShape scorefon2;
    sf::Text scoretext1;
    sf::Text scoretext2;
    sf::Text winertext;

};


#endif //PLAYINGSTATE_HPP

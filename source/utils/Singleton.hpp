#ifndef SINGLETON_HPP
#define SINGLETON_HPP


class Singleton {

public:
	Singleton(Singleton&& other) = delete;
	Singleton(const Singleton& other) = delete;

	Singleton& operator= (Singleton&& other) = delete;
	Singleton& operator= (const Singleton& other) = delete;


protected:
	Singleton() = default;
	virtual ~Singleton() = default;

};


#endif //CORE_SINGLETON_HPP

#include "TickManager.hpp"

#include <SFML/System.hpp>

#include "../Context.hpp"

TickManager::TickManager() {
	m_tickTime = 0;
}

void TickManager::run() {
	sf::Clock clock;
	sf::Time time;

	while ((*g_window).isOpen()) {

		time = clock.getElapsedTime();

		if ((time.asMicroseconds() / 50) > m_tickTime) {
			if (!m_tickObject.empty()) {
				for (auto tick : m_tickObject) {
					(*tick).TickUpdate(m_tickTime);
				}
			}
		}

		m_tickTime = (time.asMicroseconds() / 50);

		if (m_tickTime > 23999) {
			m_tickTime = 0;
			clock.restart();
		}

	}
}

int TickManager::getTickTime() {
	return m_tickTime;
}

void TickManager::add(std::shared_ptr<TickObject> tick) {
	m_tickObject.push_back(tick);
}

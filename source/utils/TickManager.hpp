#ifndef TICKMANAGER_HPP
#define TICKMANAGER_HPP

#include <memory>
#include <vector>

#include "TickObject.hpp"

class TickManager {

public:
	TickManager();

	void run();

	int getTickTime();

	void add(std::shared_ptr<TickObject> tick);

private:
	std::vector<std::shared_ptr<TickObject>> m_tickObject;

	unsigned int m_tickTime;

};


#endif //TICKMANAGER_HPP

#ifndef TICKOBJECT_HPP
#define TICKOBJECT_HPP

class TickObject {

public:
	virtual ~TickObject() = default;
	virtual void TickUpdate(unsigned int tickTime) = 0;

};

#endif //TICKOBJECT_HPP
